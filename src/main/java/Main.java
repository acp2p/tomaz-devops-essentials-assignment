public class Main {
    private static final Integer NUMBER_1 = 7;
    private static final Integer NUMBER_2 = 5;

    public static void main (String[] args) {
        Integer additionResult = Calculator.add(NUMBER_1, NUMBER_2);
        Integer multiplicationResult = Calculator.multiply(NUMBER_1, NUMBER_2);
        Double divisionResult = Calculator.divide(NUMBER_1, NUMBER_2);
        Integer subtractionResult = Calculator.subtract(NUMBER_1, NUMBER_2);

        System.out.println("********* Basic Math Operations *********");
        System.out.println(NUMBER_1 + " + " + NUMBER_2 + " = " + additionResult);
        System.out.println(NUMBER_1 + " - " + NUMBER_2 + " = " + subtractionResult);
        System.out.println(NUMBER_1 + " * " + NUMBER_2 + " = " + multiplicationResult);
        System.out.println(NUMBER_1 + " / " + NUMBER_2 + " = " + divisionResult);
        System.out.println("*****************************************");
    }
}
