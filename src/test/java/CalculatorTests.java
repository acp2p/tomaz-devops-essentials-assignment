import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTests {

    @Test
    public void addTest() {
        assertEquals(10, Calculator.add(3, 7));
    }

    @Test
    public void subTest() {
        assertEquals(2, Calculator.subtract(5, 3));
    }

    @Test
    public void multiplyTest() {
        assertEquals(8, Calculator.multiply(4, 2));
    }

    @Test
    public void divideTest() {
        assertEquals((Double) 1.5, Calculator.divide(3, 2));
    }
}
