FROM openjdk:11

RUN mkdir /usr/src/devops-essentials-assignment

WORKDIR /usr/src/devops-essentials-assignment

COPY ./build/libs/tomaz-devops-essentials-assignment-1.0-SNAPSHOT.jar /usr/src/devops-essentials-assignment

CMD ["java", "-jar", "tomaz-devops-essentials-assignment-1.0-SNAPSHOT.jar"]
